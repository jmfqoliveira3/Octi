from dataclasses import dataclass, field
from abc import ABC, abstractmethod

@dataclass
class Piece:
    id: int
    arrows: list[int] = field(default_factory=list)


@dataclass
class Square:
    pieceOn: Piece | None


class PlayerInGame(ABC):
    def __init__(self, pieces: list[Piece], starting_arrows=12):
        self._set_pieces(pieces)
        self.arrows = starting_arrows

    def _set_pieces(self, pieces: list[Piece]):
        for piece in pieces:
            setattr(self, f'piece{piece.id}', piece)
    
    def get_piece(self, piece_id: int):
        return getattr(self, f'piece{piece_id}')
    
    def put_arrow(self, piece_id: int, arrow_direction_id: int):
        piece = self.get_piece(piece_id)
        piece.arrows.append(arrow_direction_id)
        piece.arrows.sort()